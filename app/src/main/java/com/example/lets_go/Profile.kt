package com.example.lets_go

import android.net.Uri
import android.widget.ImageView

data class Profile(
    val id: String? = null,
    var title: String? = null,
    var name: String? = null,
    val imageUrl: String? = null
)

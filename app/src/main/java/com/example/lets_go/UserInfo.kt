package com.example.lets_go


data class UserInfo(
    var name: String = "",
    var surname: String = "",
    var email: String = ""
)
